

int GtextSize =26; 



void draw() {
    Bbar();
    SaveAndView(CurrentPosition);  
    //NotAvailable();
    
}


void NotAvailable(){
  
  
  strokeWeight(1.5);
  stroke(#4F4F4F); 
  fill(255);
  rect(200,165,350,180);
  
  
  fill(#1B296F);
  textSize(20);
  textAlign(CENTER); 
  text("This Function is not available",370,210);
  text("in the simulation software",370,240);
 
  strokeWeight(1.5);
  stroke(#4F4F4F); fill(255);
  ellipse(375, 300, 60, 60); // Tick button - for exit
  
}

void Bbar() {

  strokeWeight(1.5);
  stroke(255); fill(#4F4F4F);
  rect(0, 2, 50, 38); // B1
  rect(50, 2, 495, 38 ); // B2
  rect(545, 2, 50, 38); // B3
  rect( 595, 2, 203, 38 ); // B4
  
  fill(255);
  textSize(25);
  textAlign(LEFT);
  text("ICU", 5, 30);
  textAlign(RIGHT);
  
  
  int s = second();  // Values from 0 - 59
  int m = minute();  // Values from 0 - 59
  int h = hour();    // Values from 0 - 23
  String time  = h + ":" + m + ":" + s;
  text(time, 790, 30);
 
}




void NewLayer (){
  
    noStroke();
    fill(255);
    rect( 0, 40, 800, 480); // white cover for new menu 
    L(); // redisplaying the L bar 
  
}