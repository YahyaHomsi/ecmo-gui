
void DBar() {

    image(D1W,14,430,107, 41);  //D1
    image(D2W,121,430,106, 41); //D2
    image(D3W,227,430,106, 41); //D3
    image(D4W,333,430,106, 41); //D4
    image(D5Y,439,430,106, 41); //D5
    image(D6W,545,430,107, 41); //D6
}


void D6(){
  
  stroke(#4F4F4F); 
  fill(255);

  rect(15, 42, 318, 97);  
  rect(333, 42, 318, 97); 
  
  rect(15, 139, 318, 150);
  rect(15, 289, 318, 150);
  
  rect(333, 139, 318, 150);
  rect(333, 289, 318, 150);
  
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);

  rect ( 100 ,  220 , 220 , 50 , 100 ,100 , 100 ,100); 
  rect ( 100 ,  365 , 220 , 50 , 100 ,100 , 100 ,100); 

  rect ( 420 ,  220 , 220 , 50 , 100 ,100 , 100 ,100); 
  rect ( 420 ,  365 , 220 , 50 , 100 ,100 , 100 ,100); 
  
    //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);

  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT);
  
 //text(nf(LoadVariable("lpm"),0,0), 330, 130);    
  text(nf(LoadVariable("rpm"),0,0), 650, 130);

  
  
  
}
void D1() {

  stroke(#4F4F4F); 
  fill(255);
  rect(15, 42, 318, 129); // G1 
  rect( 15, 171, 318, 129); // G2
  rect( 15, 300, 318, 129); // G3

  rect( 333, 42, 318, 129); // G5
  rect( 333, 171, 318, 129); // G6
  rect( 333, 300, 318, 129); // G7

  //A 
  image(IPM,25,50,90, 50); 
  image(Pven,25,179,90, 50); 
  image(DeltaP,25,308,90, 50); 

  image(RPM,343,50,90, 50); 
  image(PArt,343,179,90, 50); 
  image(SvO2,343,308,90, 50); 
  
  strokeWeight(1.5);
  
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,80);
  text("rpm",640,80);
  
  text("mmHg",320,209);
  text("mmHg",640,209);
  
  text("mmHg",320,338);
  text("%",640,338);

  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT);
  

 // text(nf(LoadVariable("lpm"),0,0), 330, 161);  
  text(nf(LoadVariable("Pven"),0,1), 330, 290); 
  //text(nf(LoadVariable("Pint"),0,1), 330, 419);
  
  text(nf(LoadVariable("rpm"),0,0), 650, 161);
  text(nf(LoadVariable("Part"),0,1), 650, 290);
  text(nf(LoadVariable("Tart"),0,1), 650, 419);
  
}


void D2() {

       
  stroke(#4F4F4F); 
  fill(255);

  image(IPM,15,42,90, 50); 
  image(Pven,15,139,90, 50); 
  image(PInt,15,236,90, 50); 
  image(DeltaP,333,308,90, 50); 

  image(RPM,343,50,90, 50); 
  image(PArt,343,179,90, 50); 
  image(TArt,343,308,90, 50); 
  image(TArt,343,308,90, 50); 
  
  
  rect(15, 42, 318, 97); // G1 
  rect( 15, 139, 318, 97); // G2
  rect( 15, 236, 318, 97); // G3
  rect( 15, 333, 318, 97); // G4

  rect( 333, 42, 318, 97); // G5
  rect( 333, 139, 318, 97); // G6
  rect( 333, 236, 318, 97); // G7
  rect( 333, 333, 318, 97); // G8

  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 25 ,  147 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 25 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 25 ,  341 , 90 , 50 , 100 ,100 , 100 ,100);
  
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  147 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  341 , 90 , 50 , 100 ,100 , 100 ,100);
  strokeWeight(1.5);
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);
  
  text("mmHg",320,172);
  text("mmHg",640,172);
  
  text("mmHg",320,269);
  text("°C",640,269);
  
  text("mmHg",320,366);
  text("%",640,366);
 
  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT);
  
  
  //text(nf(LoadVariable("lpm"),0,0), 330, 130);  
  text(nf(LoadVariable("Pven"),0,1), 330, 229); 
  //text(nf(LoadVariable("Pint"),0,1), 330, 325);
  text( nf(LoadVariable("deltaP"),0,1), 330, 420);
  
  text(nf(LoadVariable("rpm"),0,0), 650, 130);
  text(nf(LoadVariable("Part"),0,1), 650, 229);
  text(nf(LoadVariable("Tart"),0,1), 650, 323);
  text(nf(LoadVariable("svO2"),0, 1), 650, 420);
  
}


void D3() {

  stroke(#4F4F4F); fill(255);

  rect(15, 42, 318, 97); // G1 
  rect( 15, 139, 318, 97); // G2
  rect( 15, 236, 318, 97); // G3
  rect( 15, 333, 318, 97); // G4

  rect( 333, 42, 318, 97); // G5
  rect( 333, 139, 318, 97); // G6
  rect( 333, 236, 318, 194); // G7

  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 25 ,  147 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 25 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 25 ,  341 , 90 , 50 , 100 ,100 , 100 ,100);
  
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  147 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  strokeWeight(1.5);
  
    //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);
  
  text("°C",320,172);
  text("°C",640,172);
  
  text("g/dl",320,269);
  text("%",640,269);
  
  text("%",320,366);
   
   
  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT); 
  
  
 // text(nf(LoadVariable("lpm"),0,0), 330, 130);  
  text(nf(LoadVariable("Tven"),0,1), 330, 229); 
  text(nf(LoadVariable("Hb"),0,1), 330, 325);
  text( nf(LoadVariable("Hct"),0,1), 330, 420);
  
  text(nf(LoadVariable("rpm"),0,0), 650, 130);
  text(nf(LoadVariable("Tart"),0,1), 650, 229);
  
  textSize(70);
  text(nf(LoadVariable("svO2"),0, 1), 650, 410);
  
}




void D4() {

  stroke(#4F4F4F); fill(255);

  rect(15, 42, 318, 194); // G1 
  rect( 15, 236, 318, 194); // G3

  rect( 333, 42, 318, 194); // G5
  rect( 333, 236, 318, 194); // G7

  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 25 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  strokeWeight(1.5);

  strokeWeight(1.5);
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
 // text("lpm",320,80);
  text("rpm",640,80);
  
  text("mmHg",320,274);
  text("%",640,274);
  
  
  // G Text Variables
  fill(#1B296F);
  textSize(70);
  textAlign(RIGHT);
  
 
  //text(nf(LoadVariable("lpm"),0,1), 330, 219); 
  text( nf(LoadVariable("Pven"),0,1), 330, 410);
  
  text(nf(LoadVariable("rpm"),0,1), 650, 219);
  text(nf(LoadVariable("svO2"),0, 1), 650, 410);
  
}

void D5() {
  
  noStroke();fill(255);
  rect( 333, 236, 318, 97); // G7
  
  stroke(#4F4F4F); fill(255);
  rect(15, 42, 318, 97); // G1 
  rect( 15, 139, 318, 97); // G2
  rect( 15, 236, 318, 97); // G3
  rect( 15, 333, 318, 97); // G4

  rect( 333, 42, 318, 97); // G5
  rect( 333, 139, 318, 97); // G6
  rect( 333, 333, 318, 97); // G8
  


  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 25 ,  147 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 25 ,  244 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 25 ,  341 , 90 , 50 , 100 ,100 , 100 ,100);
  
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  147 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 343 ,  341 , 90 , 50 , 100 ,100 , 100 ,100);
  strokeWeight(1.5);
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);
  
  text("mmHg",320,172);
  text("mmHg",640,172);
  
  text("mmHg",320,269);
  
  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT); 
  
  //text(nf(LoadVariable("lpm"),0,0), 330, 130);  
  text(nf(LoadVariable("Pven"),0,1), 330, 229); 
  //text(nf(LoadVariable("Pint"),0,1), 330, 325);
  
  text(nf(LoadVariable("rpm"),0,0), 650, 130);
  text(nf(LoadVariable("Part"),0,1), 650, 229);
    

}