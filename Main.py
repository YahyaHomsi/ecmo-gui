import json
from Authentication import *

### FUNCTIONS
def saveAsJSON(dictionry):
    with open('data/data.json', 'w') as fp:
        json.dump(dictionry, fp)

def sync(parameter):
    print("Parameter updated.")

    key = parameter["path"]
    value = parameter["data"]
    global parameters
    parameters = database.child("parameters").get().val()
    saveAsJSON(parameters)

    print(parameter["data"])
###

# Parameters
parameters = {"rpm": 0.0, "Pven": 0.0, "Part": 0.0, "deltaP": 0.0, "svO2": 0.0, "Tart": 0.0, "Tven": 0.0, "Hb": 0.0, "Hct": 0.0, "V": 0.0}

# Authenticate
# auth = firebase.auth()
# user = auth.sign_in_with_email_and_password("a@fkrtech.com", "pythonForECMO")
# token = user['idToken']

# Initiate database reference
database = firebase.database()

# Update parameters database
# database.child("parameters").update(parameters)
parameters = database.child("parameters").get().val()

# Listen for changes and update local parameters accordingly
parametersStream = database.child("parameters").stream(sync)