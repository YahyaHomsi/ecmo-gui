void L() {
  

  stroke(#4F4F4F);
  rect( 675, 42, 105, 73); // L1
  rect( 675, 115, 105, 73); // L2
  
  rect( 675, 200, 105, 53); // L3
  rect( 675, 260, 105, 53); // L4
  
  rect( 675, 398, 105, 73); // L5
  
}


void LMenu() {

  stroke(#4F4F4F);
  fill(255);
 
  rect( 360, 115, 315, 215); // Container
  
  rect( 375, 130, 85, 85); // LM1
  rect( 375, 230, 85, 85); // LM2
  
  rect( 475, 130, 85, 85); // LM3
  rect( 475, 230, 85, 85); // LM4
  
  rect( 575, 130, 85, 85); // LM5
  rect( 575, 230, 85, 85); // LM6
}


void LP1 (){ 
  
  NewLayer();
  
  stroke(#4F4F4F); 
  fill(255);

  rect( 15, 42, 318, 108); // G1 
  rect( 15, 150, 318, 108); // G2
  rect( 15, 258, 318, 108); // G3
  rect( 15, 366, 318, 108); // G4

  rect( 333, 42, 318, 108); // G5
  rect( 333, 150, 318, 108); // G6
  rect( 333, 258, 318, 108); // G7
  rect( 333, 366, 318, 108); // G8

  

  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 235 ,  160 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 235 ,  268 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 235 ,  375 , 90 , 50 , 100 ,100 , 100 ,100);
  
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 553 ,  160 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 553 ,  268 , 90 , 50 , 100 ,100 , 100 ,100);
  rect ( 553 ,  375 , 90 , 50 , 100 ,100 , 100 ,100);
  strokeWeight(1.5);
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);
  
  textAlign(LEFT);
  fill(#1B296F);
  text("System",25,182);
  text("infromation",25,215);
  
  text("System lock",343,182);
  
  text("Brightness/",25,289);
  text("volume",25,322);
  
  text("Service",343,289);
  
  text("Language",25,396);
  text("Time/data",343,396);
 
  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT);
  
 // text(nf(LoadVariable("lpm"),0,0), 330, 140);  
  text(nf(LoadVariable("rpm"),0,0), 650, 140);
}

void LP5 (){ 
  
  NewLayer(); 
  
  stroke(#4F4F4F); 
  fill(255);

  rect(15, 42, 318, 97); // G1 
  rect(333, 42, 318, 97); // G5
  rect(15, 179, 636, 292);// White cover 

  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);

  strokeWeight(1.5);
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);

  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT);
  
 //text(nf(LoadVariable("lpm"),0,0), 330, 130);    
  text(nf(LoadVariable("rpm"),0,0), 650, 130);
  
   fill(#4F4F4F);
   rect(15, 139, 636, 40); // gray bar
   
   fill(255);
   textSize(30);
   textAlign(LEFT);
   text("Pump settings",20,170); // label 
   
   
      fill(255);
   rect(40,230,220,220,50,50,50,50);
   line(40,280,260,280);
   line(40,400,260,400);
   
   rect(400,230,220,50,50,50,50,50);
   
   
   fill(#1B296F);
   textSize(22);
   textAlign(LEFT);
   text("Control mode:",40,215); // label 
   
   text("Pump mode:",400,215); // label 
   
   text("Continuous",450,265); // label 
   

   
  
}


int startTime = millis();
int FulBatteryTime = 60;
boolean Power = false;

void L5() {

       
  stroke(#4F4F4F); 
  fill(255);

  rect(15, 42, 318, 97); // G1 
  rect(333, 42, 318, 97); // G5
  rect(15, 179, 636, 292);// White cover 

  //GI
  strokeWeight(2.5);
  fill(255);
  rect ( 25 ,  50 , 90 , 50 , 100 ,100 , 100 ,100); 
  rect ( 343 ,  50 , 90 , 50 , 100 ,100 , 100 ,100);

  strokeWeight(1.5);
  
  //G Text
  fill(#4F4F4F);
  textSize(GtextSize);
  textAlign(RIGHT);
 
  text("lpm",320,75);
  text("rpm",640,75);

  // G Text Variables
  fill(#1B296F);
  textSize(60);
  textAlign(RIGHT);
  
 //text(nf(LoadVariable("lpm"),0,0), 330, 130);    
  text(nf(LoadVariable("rpm"),0,0), 650, 130);
  
   fill(#4F4F4F);
   rect(15, 139, 636, 40); // gray bar
   
   fill(255);
   textSize(30);
   textAlign(LEFT);
   text("Battery",20,170); // label 
   
   ellipse(600, 430, 60, 60); // Tick button - for exit
   
   
    
  if ( Power){
   int RemainingBatteryTime = FulBatteryTime - ((millis() - startTime )/60000);
   int hours=(RemainingBatteryTime/60);
   int min=(RemainingBatteryTime%60);

   fill(#1B296F);
   textSize(40);
   textAlign(CENTER);
    
   text( hours +"h : "+min+"min",520,270); //  battery
  }
  
   else {
   fill(#1B296F);
   textSize(40);
   textAlign(CENTER);
   text( "--h : --min",520,270); // battery 
   }
    
}